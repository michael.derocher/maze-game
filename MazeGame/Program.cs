﻿using MazeGame;
using System;
using System.Windows.Forms;

internal class Program
{
    [STAThread]
    private static void Main(string[] args)
    {
        var game = new MazeGame.MazeGame();
        game.Run();
    }
}