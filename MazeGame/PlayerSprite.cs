﻿using Maze;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MazeGame
{
    public class PlayerSprite : DrawableGameComponent
    {
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private MazeGame _game;
        private InputManager _inputManager;
        private SpriteBatch _spriteBatch;
        private Map _map;
        private Vector2 _playerPosition;
        private MapVector _previousPosition;
        private Direction _previousFacing;
        private Texture2D _playerTexture;
        private Texture2D _pathTexture;
        private bool _firstDraw;

        public PlayerSprite(Game game, Map map) : base(game)
        {
            _game = (MazeGame)game;
            _map = map;

            _inputManager = InputManager.Instance;
            _inputManager.RemoveKeyHandlers();

            _inputManager.AddKeyHandler(Keys.W, ForwardAndLog);
            _inputManager.AddKeyHandler(Keys.S, BackwardAndLog);
            _inputManager.AddKeyHandler(Keys.A, LeftAndLog);
            _inputManager.AddKeyHandler(Keys.D, RightAndLog);

            _inputManager.AddKeyHandler(Keys.Up, ForwardAndLog);
            _inputManager.AddKeyHandler(Keys.Down, BackwardAndLog);
            _inputManager.AddKeyHandler(Keys.Left, LeftAndLog);
            _inputManager.AddKeyHandler(Keys.Right, RightAndLog);

        }

        private void ForwardAndLog()
        {
            _map.Player.MoveForward();
            _logger.Info($"Player moved forward to [X: {_map.Player.Position.X}, Y: {_map.Player.Position.Y}]");
        }

        private void BackwardAndLog()
        {
            _map.Player.MoveBackward();
            _logger.Info($"Player moved backward to [X: {_map.Player.Position.X}, Y: {_map.Player.Position.Y}]");
        }

        private void LeftAndLog()
        {
            _map.Player.TurnLeft();
            _logger.Info($"Player turned left. Now facing : {_map.Player.Facing}");
        }

        private void RightAndLog()
        {
            _map.Player.TurnRight();
            _logger.Info($"Player turned right. Now facing : {_map.Player.Facing}");
        }

        public override void Initialize()
        {
            _playerPosition = new Vector2((_map.Player.Position.X * 32) + 16, (_map.Player.Position.Y * 32) + 16);
            _previousPosition = _map.Player.Position;
            _previousFacing = _map.Player.Facing;
            _firstDraw = true;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = _spriteBatch = new SpriteBatch(GraphicsDevice);

            _playerTexture = Game.Content.Load<Texture2D>("rat");
            _pathTexture = Game.Content.Load<Texture2D>("path");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            _inputManager.Update();
            _playerPosition = new Vector2((_map.Player.Position.X * 32) + 16, (_map.Player.Position.Y * 32) + 16);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            if (_firstDraw && _game.MazeHasRendered)
            {
                _spriteBatch.Begin();
                _spriteBatch.Draw(_playerTexture, _playerPosition, new Rectangle(0, 0, 32, 32), Color.White, _map.Player.GetRotation(), new Vector2(16, 16), 1.0f, SpriteEffects.None, 1);
                _logger.Debug("Drew player texture");
                _spriteBatch.End();
                _firstDraw = false;
            }

            if (_previousFacing != _map.Player.Facing || _previousPosition != _map.Player.Position)
            {
                _spriteBatch.Begin();
                _spriteBatch.Draw(_pathTexture, new Vector2((_previousPosition.X * 32), (_previousPosition.Y * 32)), Color.White);
                _logger.Debug("Drew path texture");
                _spriteBatch.Draw(_playerTexture, _playerPosition, new Rectangle(0, 0, 32, 32), Color.White, _map.Player.GetRotation(), new Vector2(16, 16), 1.0f, SpriteEffects.None, 1);
                _logger.Debug("Drew player texture");
                _spriteBatch.End();
            }

            _previousPosition = _map.Player.Position;
            _previousFacing = _map.Player.Facing;

            base.Draw(gameTime);
        }

    }
}
