﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeGame
{
    public class InputManager
    {
        private static Dictionary<Keys, List<Action>> _keyActions = new Dictionary<Keys, List<Action>>();
        private static Keys[] _previouslyPressed = new Keys[0];

        private static InputManager _instance = null;
        public static InputManager Instance { get {
                if (_instance == null){
                    _instance = new InputManager();
                }
                return _instance;
            }
        }

        private InputManager() { }

        public void AddKeyHandler(Keys key, Action action) {
            if (!_keyActions.ContainsKey(key)) {
                _keyActions.TryAdd(key, new List<Action>());
            }
            _keyActions[key].Add(action);
        }

        public void Update() {
            Keys[] pressedKeys = Keyboard.GetState().GetPressedKeys();
            foreach (Keys key in pressedKeys)
            {
                if (_keyActions.ContainsKey(key) && !KeyWasHeld(key)) {
                    foreach (Action action in _keyActions[key]) {
                        action.Invoke();
                    }
                }
            }
            _previouslyPressed = pressedKeys;
        }

        private bool KeyWasHeld(Keys key)
        {
            foreach (Keys previousKey in _previouslyPressed)
            {
                if (key == previousKey)
                {
                    return true;
                }
            }
            return false;
        }

        public void RemoveKeyHandlers() {
            _keyActions.Clear();
        }
    }
}
