﻿using Maze;
using Microsoft.VisualBasic.Devices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.IO;
using Microsoft.Xna.Framework.Input;
using System.Windows.Forms;
using Keys = Microsoft.Xna.Framework.Input.Keys;

namespace MazeGame;

public class MazeGame : Game
{
    enum GameState { Menu, SetupRecursive, SetupHuntKill, Playing }
    enum SelectedMenuItem { MazeFromFile, Recursive, HuntKill, MazeWidth, MazeHeight, Default, Custom }
    private GameState _gameState;
    private SelectedMenuItem _selectedMenuItem;
    private InputManager _inputManager;
    private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
    private GraphicsDeviceManager _graphics;
    private SpriteBatch _spriteBatch;
    private Map _map;
    private PlayerSprite _playerSprite;
    private Texture2D _wallTexture;
    private Texture2D _pathTexture;
    private Texture2D _goalTexture;
    private Texture2D _menuTexture;
    private SpriteFont _font;
    private int _mazeWidth;
    private int _mazeHeight;
    private bool _menuNeedsRedraw;

    public bool MazeHasRendered { get; private set; }
    public MazeGame()
    {
        _graphics = new GraphicsDeviceManager(this);
        Content.RootDirectory = "Content";
        IsMouseVisible = true;
        _inputManager = InputManager.Instance;
    }


    protected override void Initialize()
    {
        SetupMenuScreen();

        base.Initialize();
    }

    protected override void LoadContent()
    {
        _spriteBatch = new SpriteBatch(GraphicsDevice);

        _wallTexture = Content.Load<Texture2D>("wall");
        _pathTexture = Content.Load<Texture2D>("path");
        _goalTexture = Content.Load<Texture2D>("goal");
        _menuTexture = Content.Load<Texture2D>("menu");
        _font = Content.Load<SpriteFont>("font");

        base.LoadContent();
    }

    protected override void Update(GameTime gameTime)
    {
        if (_gameState == GameState.Playing && _map.IsGameFinished) {
            _logger.Info("Player has reached the goal. Returning to menu...");
            SetupMenuScreen();
        }

        _inputManager.Update();

        base.Update(gameTime);
    }

    protected override void Draw(GameTime gameTime)
    {
        if(_gameState != GameState.Playing && _menuNeedsRedraw)
        {
            if (_gameState == GameState.Menu)
            {
                DrawMenu();
            }
            else if (_gameState == GameState.SetupRecursive)
            {
                DrawSetupOptions("Recursive Maze");
            }
            else if (_gameState == GameState.SetupHuntKill)
            {
                DrawSetupOptions("Hunt & Kill Maze");
            }
            _menuNeedsRedraw = false;
        }

        base.Draw(gameTime);
    }

    private void SetupMenuScreen() {
        _logger.Info("Setting up menu screen.");
        _graphics.PreferredBackBufferWidth = 800;
        _graphics.PreferredBackBufferHeight = 480;
        _graphics.ApplyChanges();
        _gameState = GameState.Menu;
        _selectedMenuItem = SelectedMenuItem.MazeFromFile;
        _inputManager.RemoveKeyHandlers();
        _inputManager.AddKeyHandler(Keys.Enter, () => {
            if (_gameState == GameState.Menu) {
                if (_selectedMenuItem == SelectedMenuItem.MazeFromFile)
                {
                    InitializeMazeFromFile();
                    _logger.Info("Maze from file selected.");
                }
                else if (_selectedMenuItem == SelectedMenuItem.Recursive)
                {
                    _gameState = GameState.SetupRecursive;
                    SetupOptionsScreen();
                    _logger.Info("Recursive map generation selected.");
                } else if (_selectedMenuItem == SelectedMenuItem.HuntKill) {
                    _gameState = GameState.SetupHuntKill;
                    SetupOptionsScreen();
                    _logger.Info("Hunt & Kill map generation selected.");
                }
            }
        });

        _inputManager.AddKeyHandler(Keys.W, MainMenuUp);
        _inputManager.AddKeyHandler(Keys.Up, MainMenuUp);
        _inputManager.AddKeyHandler(Keys.S, MainMenuDown);
        _inputManager.AddKeyHandler(Keys.Down, MainMenuDown);
        _inputManager.AddKeyHandler(Keys.Escape, () => {
            _logger.Info("Escape key pressed. Exiting program...");
            Exit();
        });

        _menuNeedsRedraw = true;
    }

    private void DrawMenu() {
        _spriteBatch.Begin();
        _spriteBatch.Draw(_menuTexture, new Vector2(0,0), Color.White);
        _spriteBatch.DrawString(_font, "Maze Game", new Vector2(25, 25), Color.White);
        if (_selectedMenuItem == SelectedMenuItem.MazeFromFile)
        {
            _spriteBatch.DrawString(_font, "> Load from file", new Vector2(25, 125), Color.Red, 0, Vector2.One, 0.5f, SpriteEffects.None, 1f);
        }
        else
        {
            _spriteBatch.DrawString(_font, "Load from file", new Vector2(25,125), Color.White, 0, Vector2.One, 0.5f, SpriteEffects.None, 1f);
        }

        if (_selectedMenuItem == SelectedMenuItem.Recursive)
        {
            _spriteBatch.DrawString(_font, "> Recursive", new Vector2(25, 175), Color.Red, 0, Vector2.One, 0.5f, SpriteEffects.None, 1f);
        }
        else
        {
            _spriteBatch.DrawString(_font, "Recursive", new Vector2(25, 175), Color.White, 0, Vector2.One, 0.5f, SpriteEffects.None, 1f);
        }

        if (_selectedMenuItem == SelectedMenuItem.HuntKill)
        {
            _spriteBatch.DrawString(_font, "> Hunt & Kill", new Vector2(25, 225), Color.Red, 0, Vector2.One, 0.5f, SpriteEffects.None, 1f);
        }
        else
        {
            _spriteBatch.DrawString(_font, "Hunt & Kill", new Vector2(25, 225), Color.White, 0, Vector2.One, 0.5f, SpriteEffects.None, 1f);
        }

        _spriteBatch.End();
    }

    private void MainMenuDown() {
        switch (_selectedMenuItem) {
            case SelectedMenuItem.MazeFromFile:
                {
                    _selectedMenuItem = SelectedMenuItem.Recursive;
                    break;
                }
            case SelectedMenuItem.Recursive:
                {
                    _selectedMenuItem = SelectedMenuItem.HuntKill;
                    break;
                }
            case SelectedMenuItem.HuntKill:
                {
                    _selectedMenuItem = SelectedMenuItem.MazeFromFile;
                    break;
                }
        }
        _logger.Info($"Main Menu option down. Selected {_selectedMenuItem}");
        _menuNeedsRedraw = true;
    }

    private void MainMenuUp()
    {
        switch (_selectedMenuItem)
        {
            case SelectedMenuItem.MazeFromFile:
                {
                    _selectedMenuItem = SelectedMenuItem.HuntKill;
                    break;
                }
            case SelectedMenuItem.Recursive:
                {
                    _selectedMenuItem = SelectedMenuItem.MazeFromFile;
                    break;
                }
            case SelectedMenuItem.HuntKill:
                {
                    _selectedMenuItem = SelectedMenuItem.Recursive;
                    break;
                }
        }
        _logger.Info($"Main Menu option up. Selected {_selectedMenuItem}");
        _menuNeedsRedraw = true;
    }

    private void DrawSetupOptions(string title)
    {
        _spriteBatch.Begin();
        _spriteBatch.Draw(_menuTexture, new Vector2(0, 0), Color.White);
        _spriteBatch.DrawString(_font, title, new Vector2(25, 25), Color.White);
        if (_selectedMenuItem != SelectedMenuItem.Default)
        {
            _spriteBatch.DrawString(_font, "Default", new Vector2(25, 125), Color.White, 0, Vector2.One, 0.5f, SpriteEffects.None, 1f);
        }

        if (_selectedMenuItem != SelectedMenuItem.Custom)
        {
            _spriteBatch.DrawString(_font, "Custom", new Vector2(25, 175), Color.White, 0, Vector2.One, 0.5f, SpriteEffects.None, 1f);
        }

        if (_selectedMenuItem != SelectedMenuItem.MazeWidth)
        {
            _spriteBatch.DrawString(_font, $"Width: {(_mazeWidth * 2) + 1}", new Vector2(50, 215), Color.White, 0, Vector2.One, 0.35f, SpriteEffects.None, 1f);
        }

        if (_selectedMenuItem != SelectedMenuItem.MazeHeight)
        {
            _spriteBatch.DrawString(_font, $"Height: {(_mazeHeight * 2) + 1}", new Vector2(50, 240), Color.White, 0, Vector2.One, 0.35f, SpriteEffects.None, 1f);
        }

        switch (_selectedMenuItem)
        {
            case SelectedMenuItem.Default:
                {
                    _spriteBatch.DrawString(_font, "> Default", new Vector2(25, 125), Color.Red, 0, Vector2.One, 0.5f, SpriteEffects.None, 1f);
                    break;
                }
            case SelectedMenuItem.Custom:
                {
                    _spriteBatch.DrawString(_font, "> Custom", new Vector2(25, 175), Color.Red, 0, Vector2.One, 0.5f, SpriteEffects.None, 1f);
                    break;
                }
            case SelectedMenuItem.MazeWidth:
                {
                    _spriteBatch.DrawString(_font, $"Width: < {(_mazeWidth * 2) + 1} >", new Vector2(50, 215), Color.Red, 0, Vector2.One, 0.35f, SpriteEffects.None, 1f);
                    break;
                }
            case SelectedMenuItem.MazeHeight:
                {
                    _spriteBatch.DrawString(_font, $"Height: < {(_mazeHeight * 2) + 1} >", new Vector2(50, 240), Color.Red, 0, Vector2.One, 0.35f, SpriteEffects.None, 1f);
                    break;
                }
        }

        _spriteBatch.End();
    }

    private void SetupOptionsScreen()
    {
        _logger.Info("Setting up maze options screen.");
        _selectedMenuItem = SelectedMenuItem.Default;
        _mazeWidth = 2;
        _mazeHeight = 2;
        _inputManager.RemoveKeyHandlers();
        _inputManager.AddKeyHandler(Keys.Enter, () => {
            if (_selectedMenuItem == SelectedMenuItem.Default || _selectedMenuItem == SelectedMenuItem.Custom)
            {
                _logger.Info($"Initializing maze.");
                if (_gameState == GameState.SetupRecursive)
                {
                    InitializeRecursiveMaze();
                }
                else if (_gameState == GameState.SetupHuntKill)
                {
                    InitializeHuntKillMaze();
                }

            }
        });

        _inputManager.AddKeyHandler(Keys.W, SetupMenuUp);
        _inputManager.AddKeyHandler(Keys.Up, SetupMenuUp);
        _inputManager.AddKeyHandler(Keys.S, SetupMenuDown);
        _inputManager.AddKeyHandler(Keys.Down, SetupMenuDown);

        _inputManager.AddKeyHandler(Keys.A, DecrementMazeSize);
        _inputManager.AddKeyHandler(Keys.Left, DecrementMazeSize);
        _inputManager.AddKeyHandler(Keys.D, IncrementMazeSize);
        _inputManager.AddKeyHandler(Keys.Right, IncrementMazeSize);

        _inputManager.AddKeyHandler(Keys.Escape, () => {
            _logger.Info("Escape key pressed. Returning to Menu...");
            SetupMenuScreen();
        });

        _menuNeedsRedraw = true;
    }

    private void IncrementMazeSize()
    {
        if (_selectedMenuItem == SelectedMenuItem.MazeWidth)
        {
            _mazeWidth++;
        }
        else if (_selectedMenuItem == SelectedMenuItem.MazeHeight)
        {
            _mazeHeight++;
        }

        _logger.Info($"Incremented {_selectedMenuItem}. Maze width: {_mazeWidth}, Maze height: {_mazeHeight}");
        _menuNeedsRedraw = true;
    }

    private void DecrementMazeSize()
    {
        if (_selectedMenuItem == SelectedMenuItem.MazeWidth && _mazeWidth > 2)
        {
            _mazeWidth--;
        }
        else if (_selectedMenuItem == SelectedMenuItem.MazeHeight && _mazeHeight > 2)
        {
            _mazeHeight--;
        }

        _logger.Info($"Decremented {_selectedMenuItem}. Maze width: {_mazeWidth}, Maze height: {_mazeHeight}");
        _menuNeedsRedraw = true;
    }

    private void SetupMenuUp()
    {
        switch (_selectedMenuItem)
        {
            case SelectedMenuItem.Default:
                {
                    _selectedMenuItem = SelectedMenuItem.MazeHeight;
                    break;
                }
            case SelectedMenuItem.Custom:
                {
                    _selectedMenuItem = SelectedMenuItem.Default;
                    break;
                }
            case SelectedMenuItem.MazeWidth:
                {
                    _selectedMenuItem = SelectedMenuItem.Custom;
                    break;
                }
            case SelectedMenuItem.MazeHeight:
                {
                    _selectedMenuItem = SelectedMenuItem.MazeWidth;
                    break;
                }
        }
        _logger.Info($"Setup Menu option up. Selected {_selectedMenuItem}");
        _menuNeedsRedraw = true;
    }

    private void SetupMenuDown()
    {
        switch (_selectedMenuItem)
        {
            case SelectedMenuItem.Default:
                {
                    _selectedMenuItem = SelectedMenuItem.Custom;
                    break;
                }
            case SelectedMenuItem.Custom:
                {
                    _selectedMenuItem = SelectedMenuItem.MazeWidth;
                    break;
                }
            case SelectedMenuItem.MazeWidth:
                {
                    _selectedMenuItem = SelectedMenuItem.MazeHeight;
                    break;
                }
            case SelectedMenuItem.MazeHeight:
                {
                    _selectedMenuItem = SelectedMenuItem.Default;
                    break;
                }
        }
        _logger.Info($"Setup Menu option down. Selected {_selectedMenuItem}");
        _menuNeedsRedraw = true;
    }

    private void InitializeRecursiveMaze() {
        _logger.Info("Initializing Recursive Maze.");
        IMapProvider provider = MapProviderCreator.CreateMapProvider("RecursiveMazeGenerator");
        _map = new Map(provider);

        MazeHasRendered = false;
        InitializeMap();
    }

    private void InitializeHuntKillMaze() {
        _logger.Info("Initializing Recursive Maze.");
        IMapProvider provider = HuntKillCreator.CreateImprovedHuntKill();
        _map = new Map(provider);

        MazeHasRendered = false;
        InitializeMap();
    }

    private void InitializeMazeFromFile() {
        _logger.Info("Initializing Maze From File.");
        OpenFileDialog dialog = new OpenFileDialog();
        dialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
        dialog.Title = "Please select a Text file";
        String file = "";

        dialog.ShowDialog();
        file = dialog.FileName;

        if (file != "")
        {
            try
            {
                IMapProvider provider = new MazeFromFile.MazeFromFile(file);
                _map = new Map(provider);
                _logger.Info($"Map \"{file}\" selected from disk.");
            }
            catch (Exception e)
            {
                _logger.Error(e);
            }

            MazeHasRendered = false;
            _selectedMenuItem = SelectedMenuItem.Default;
            InitializeMap();
        }
    }

    private void InitializeMap() {
        try
        {
            _logger.Info($"Loading map...");
            if(_selectedMenuItem == SelectedMenuItem.Default)
            {
                _map.CreateMap();
            }
            else
            {
                _map.CreateMap(_mazeWidth, _mazeHeight);
            }

            _logger.Info("Map successfully created.");
        }
        catch (Exception e)
        {
            _logger.Error($"Failed to load map.");
            _logger.Error(e.Message);
            Exit();
        }
        _gameState = GameState.Playing;
        _graphics.PreferredBackBufferHeight = _map.MapGrid.GetLength(0) * 32;
        _graphics.PreferredBackBufferWidth = _map.MapGrid.GetLength(1) * 32;
        _graphics.ApplyChanges();

        _inputManager.RemoveKeyHandlers();
        _playerSprite = new PlayerSprite(this, _map);
        _inputManager.AddKeyHandler(Keys.Escape, () => {
            _logger.Info("Escape key pressed. Returning to Menu...");
            SetupMenuScreen();
        });
        Components.Add(_playerSprite);

        _logger.Info($"Player starting at [X: {_map.Player.StartX}, Y: {_map.Player.StartY}] and facing {_map.Player.Facing}.");
        _logger.Info($"Goal located at [X: {_map.Goal.X}, Y: {_map.Goal.Y}].");

        _spriteBatch.Begin();
        for (int y = 0; y < _map.MapGrid.GetLength(0); y++)
        {
            for (int x = 0; x < _map.MapGrid.GetLength(1); x++)
            {
                if (_map.MapGrid[y, x] == Block.Solid)
                {
                    _spriteBatch.Draw(_wallTexture, new Vector2((x * 32), (y * 32)), Color.White);
                    _logger.Debug("Drew wall texture");
                }
                else
                {
                    _spriteBatch.Draw(_pathTexture, new Vector2((x * 32), (y * 32)), Color.White);
                    _logger.Debug("Drew path texture");
                }
            }
        }
        _spriteBatch.Draw(_goalTexture, new Vector2((_map.Goal.X * 32), (_map.Goal.Y * 32)), Color.White);
        _logger.Debug("Drew goal texture");
        _spriteBatch.End();

        MazeHasRendered = true;
    }
}
