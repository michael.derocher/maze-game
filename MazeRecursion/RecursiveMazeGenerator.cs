﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("MazeRecursionTests")]
namespace Maze
{

    internal class RecursiveMazeGenerator : IMapProvider
    {
        private List<MapVector> _visitedVectors;
        private Random _random;

        public RecursiveMazeGenerator(int? seed = null)
        {
            if (seed != null)
            {
                _random = new Random((int)seed);
            }
            else
            {
                _random = new Random();
            }
        }

        public Direction[,] CreateMap(int width, int height)
        {
            if (width < 0 || height < 0)
            {
                throw new ArgumentException("Width and height cannot be negative.");
            }
            Direction[,] grid = new Direction[height, width];
            for (int y = 0; y < grid.GetLength(0); y++) {
                for (int x = 0; x < grid.GetLength(1); x++) {
                    grid[y, x] = Direction.None;
                }
            }


            MapVector randomVector = new MapVector(_random.Next(width), _random.Next(height));
            _visitedVectors = new List<MapVector>();

            return Walk(grid, randomVector);
        }

        public Direction[,] CreateMap()
        {
            return CreateMap(8, 6);
        }

        private Direction[,] Walk(Direction[,] grid, MapVector position, Direction comingFrom = Direction.None) {
            if (!_visitedVectors.Contains(position))
            {
                MapVector previousPosition = position - comingFrom;
                grid[previousPosition.Y, previousPosition.X] = grid[previousPosition.Y, previousPosition.X] | comingFrom;
                grid[position.Y, position.X] = grid[position.Y, position.X] | OppositeDirection(comingFrom);
                _visitedVectors.Add(position);
                List<Direction> possibleDirections = RandomDirections(grid, position);
                foreach (Direction direction in possibleDirections)
                {
                    grid = Walk(grid, position + direction, direction);
                }
            }
            return grid;
        }

        private static Direction OppositeDirection(Direction direction) {
            switch (direction)
            {
                case Direction.N:
                    return Direction.S;
                case Direction.E:
                    return Direction.W;
                case Direction.S:
                    return Direction.N;
                case Direction.W:
                    return Direction.E;
                default:
                    return Direction.None;
            }
        }

        private List<Direction> RandomDirections(Direction[,] grid, MapVector position) {
            List<Direction> possibleDirections = new List<Direction>();
            if (position.X > 0)
            {
                possibleDirections.Add(Direction.W);
            }

            if (position.X < grid.GetLength(1) - 1)
            {
                possibleDirections.Add(Direction.E);
            }

            if (position.Y > 0)
            {
                possibleDirections.Add(Direction.N);
            }

            if (position.Y < grid.GetLength(0) - 1)
            {
                possibleDirections.Add(Direction.S);
            }

            List<Direction> result = new List<Direction>();
            int initialCount = possibleDirections.Count;
            for (int i = 0; i < initialCount; i++) {
                int randomIndex = _random.Next(possibleDirections.Count);
                result.Add(possibleDirections[randomIndex]);
                possibleDirections.RemoveAt(randomIndex);
            }
            

            return result;
        }
    }
}
