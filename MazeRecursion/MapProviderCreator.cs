﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maze
{
    public class MapProviderCreator
    {

        public static IMapProvider CreateMapProvider(string type) {
            switch (type)
            {
                case "RecursiveMazeGenerator":
                    return new RecursiveMazeGenerator();
                default:
                    return new RecursiveMazeGenerator();
            }
        }

    }
}
