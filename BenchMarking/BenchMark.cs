﻿using Maze;
using System.Diagnostics;

internal class BenchMark
{
    public delegate IMapProvider ProviderProvider();
    private static void Main(string[] args)
    {
        ProviderProvider huntKill = () => { return HuntKillCreator.CreateHuntKill(); };
        ProviderProvider improvedHuntKill = () => { return HuntKillCreator.CreateImprovedHuntKill(); };
        ProviderProvider recursive = () => { return MapProviderCreator.CreateMapProvider("RecursiveMazeGenerator"); };

        Console.WriteLine("\n----------- Improved Hunt & Kill benchmark -----------");
        runBenchmark(improvedHuntKill, "improvedHuntkillBenchmark.csv", 50);
        Console.WriteLine("\n----------- Original Hunt & Kill benchmark -----------");
        runBenchmark(huntKill, "huntkillBenchmark.csv", 20);
        Console.WriteLine("\n----------- Recursive benchmark -----------");
        runBenchmark(recursive, "recursiveBenchmark.csv", 9);

    }

    public static void runBenchmark(ProviderProvider providerProvider, string outputFile, int repeats) {
        bool validInput = false;
        string userInput = "";
        while (!validInput)
        {
            Console.WriteLine($"Input directory path to store {outputFile} : ");
            userInput = Console.ReadLine();
            if (Directory.Exists(userInput))
            {
                validInput = true;
            }
            else
            {
                Console.WriteLine("Directory does not exists. Please try again.");
            }

        }

        string benchmarkOuput = joinFilePath(userInput, outputFile);
        if (File.Exists(benchmarkOuput))
        {
            File.Delete(benchmarkOuput);
            File.AppendAllText(benchmarkOuput, "Maze Size, Time (ms) \n");
        }
        else
        {
            File.AppendAllText(benchmarkOuput, "Maze Size, Time (ms) \n");
        }

        Console.WriteLine("Benchmarking...");

        Parallel.For(1, repeats + 1, i =>
        {
            IMapProvider? provider = providerProvider?.Invoke();
            if (provider != null)
            {
                int size = i * 10;
                Console.WriteLine($"Running size {size}x{size}");
                double result = runningSumAvg(size, 100, provider);
                WriteToFile(benchmarkOuput, $"{size},{result} \n");
            }
        });
        Console.WriteLine("Benchmark finished!");

    }

    public static double runningSumAvg(int size, int n, IMapProvider provider) {
        double sum = 0;
        for (int i = 1; i <= n; i++) {
            TimeSpan result = TimeIt(() => provider.CreateMap(size, size));
            sum += result.TotalMilliseconds;
        }
        return sum/n;
    }

    public static TimeSpan TimeIt(Action bench) {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        bench?.Invoke();
        stopwatch.Stop();
        return stopwatch.Elapsed;
    }

    public static void WriteToFile(string path, string datarow) {
        File.AppendAllText(path, datarow);
    }

    public static string joinFilePath(string directory, string filename) {
        if (Path.EndsInDirectorySeparator(directory))
        {
            return directory + filename;
        }
        else 
        {
            return $"{directory}\\{filename}";
        }
    }

}