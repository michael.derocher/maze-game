﻿
using System.Diagnostics;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("MazeHuntKillTests")]
namespace Maze;
internal class HuntKillMazeGenerator : IMapProvider
{
    private Direction[,] _map;
    private Random _random;

    public HuntKillMazeGenerator(int? seed = null)
    {
        if (seed != null) {
            _random = new Random((int)seed);
        } else {
            _random = new Random();
        }
    }

    public Direction[,] CreateMap(int width, int height)
    {
        if (width < 0 || height < 0)
        {
            throw new ArgumentException("Width and height cannot be negative.");
        }
        _map = new Direction[height, width];
        Debug.Assert(_map.GetLength(0) == height && _map.GetLength(1) == width);
        for (int y = 0; y < _map.GetLength(0); y++)
        {
            for (int x = 0; x < _map.GetLength(1); x++)
            {
                _map[y, x] = Direction.None;
            }
        }
        MapVector randomVector = new MapVector(_random.Next(width), _random.Next(height));
        MapVector nextPosition = Walk(randomVector);
        do {
            while (nextPosition.X != -1 || nextPosition.Y != -1) {
                nextPosition = Walk(nextPosition);
            }
            nextPosition = Hunt();
        } while (nextPosition.X != -1 || nextPosition.Y != -1);

        return _map;
    }

    public Direction[,] CreateMap()
    {
        return CreateMap(8, 6);
    }

    private MapVector Walk(MapVector vector)
    {
        List<Direction> possibleDirections = getPossibleDirections(vector);
        if (possibleDirections.Count == 0) {
            return new MapVector(-1, -1);
        }
        int randomIndex = _random.Next(possibleDirections.Count);
        MapVector nextPosition = vector + possibleDirections[randomIndex];
        _map[vector.Y, vector.X] = _map[vector.Y, vector.X] | possibleDirections[randomIndex];
        _map[nextPosition.Y, nextPosition.X] = _map[nextPosition.Y, nextPosition.X] | OppositeDirection(possibleDirections[randomIndex]);
        return nextPosition;
    }

    private MapVector Hunt() {
        for (int y = 0; y < _map.GetLength(0); y++) {
            for (int x = 0; x < _map.GetLength(1); x++) {
                if (_map[y,x] == Direction.None) {
                    Direction direction = findAdjacentVisitedPosition(new MapVector(x, y));
                    if (direction != Direction.None) {
                        _map[y,x] = _map[y,x] | direction;
                        MapVector emptyPosition = new MapVector(x, y);
                        Direction opposite = OppositeDirection(direction);
                        MapVector adjacentPosition = emptyPosition + direction;
                        _map[adjacentPosition.Y, adjacentPosition.X] = _map[adjacentPosition.Y, adjacentPosition.X] | opposite;
                        return emptyPosition;
                    }
                }
            }
        }
        return new MapVector(-1,-1);
    }

    private Direction findAdjacentVisitedPosition(MapVector vector) {
        List<Direction> possibleDirections = new List<Direction>();
        if (vector.Y - 1 > 0 && _map[vector.Y - 1, vector.X] != Direction.None) {
                possibleDirections.Add(Direction.N);
            }
        if (vector.Y + 1 < _map.GetLength(0) &&  _map[vector.Y + 1, vector.X] != Direction.None) {
                possibleDirections.Add(Direction.S);
            }
        if (vector.X - 1 > 0 && _map[vector.Y, vector.X - 1] != Direction.None) {
                possibleDirections.Add(Direction.W);
        }
        if (vector.X + 1 < _map.GetLength(1) && _map[vector.Y, vector.X + 1] != Direction.None)
        {
            possibleDirections.Add(Direction.E);
        }

        if (possibleDirections.Count != 0) {
            return possibleDirections[_random.Next(possibleDirections.Count)];
        }
        return Direction.None;
    }

    private static Direction OppositeDirection(Direction direction)
    {
        switch (direction)
        {
            case Direction.N:
                return Direction.S;
            case Direction.E:
                return Direction.W;
            case Direction.S:
                return Direction.N;
            case Direction.W:
                return Direction.E;
            default:
                return Direction.None;
        }
    }

    private List<Direction> getPossibleDirections(MapVector position)
    {
        List<Direction> possibleDirections = new List<Direction>();
        if (position.X - 1 >= 0 && _map[position.Y,(position + Direction.W).X] == Direction.None)
        {
            possibleDirections.Add(Direction.W);
        }

        if (position.X + 1 < _map.GetLength(1) && _map[position.Y, (position + Direction.E).X] == Direction.None)
        {
            possibleDirections.Add(Direction.E);
        }

        if (position.Y - 1 >= 0 && _map[(position + Direction.N).Y, position.X] == Direction.None)
        {
            possibleDirections.Add(Direction.N);
        }

        if (position.Y + 1 < _map.GetLength(0) && _map[(position + Direction.S).Y, position.X] == Direction.None)
        {
            possibleDirections.Add(Direction.S);
        }

        return possibleDirections;
    }

}
