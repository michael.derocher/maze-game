﻿using Maze;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maze
{
    public static class HuntKillCreator
    {

        public static IMapProvider CreateHuntKill() {
            return new HuntKillMazeGenerator();
        }

        public static IMapProvider CreateImprovedHuntKill()
        {
            return new ImprovedHuntKill();
        }

    }
}
