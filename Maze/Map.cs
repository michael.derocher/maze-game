﻿namespace Maze
{
    public class Map : IMap
    {
        public MapVector Goal { get; private set; }

        public int Height { get; private set; }

        public bool IsGameFinished { get {
                return Player.Position.Equals(Goal);
            } }

        public Block[,] MapGrid { get; private set; }

        public IPlayer Player { get; private set; }

        public int Width { get; private set; }

        private IMapProvider _provider { get; }

        public Map(IMapProvider provider) {
            _provider = provider;
        }

        private Direction[,] _directionGrid;

        public void CreateMap()
        {
            //Instantiates the MapGrid
            if(_directionGrid == null) {
                _directionGrid = _provider.CreateMap();
            }
            Width = (_directionGrid.GetLength(1) * 2) + 1;
            Height = (_directionGrid.GetLength(0) * 2) + 1;
            MapGrid = new Block[Height,Width];

            fillWithSolid();

            //Sets the empty spaces in MapGrid based on the DirectionGrid
            //Also stores location of all empty spaces and dead ends in arrays for later use
            List<MapVector> emptySpaces = new List<MapVector>();
            List<MapVector> deadEnds = new List<MapVector>();
            int mapGridX, mapGridY;
            for (int y = 0; y < _directionGrid.GetLength(0); y++){
                for (int x = 0; x < _directionGrid.GetLength(1); x++) {
                    mapGridX = (x * 2) + 1;
                    mapGridY = (y * 2) + 1;
                    MapGrid[mapGridY, mapGridX] = Block.Empty;
                    emptySpaces.Add(new MapVector(mapGridX, mapGridY));
                    if (_directionGrid[y, x] == Direction.N || _directionGrid[y, x] == Direction.E || _directionGrid[y, x] == Direction.S || _directionGrid[y, x] == Direction.W)
                    {
                        deadEnds.Add(new MapVector(mapGridX, mapGridY));
                    }
                    if ((_directionGrid[y,x] & Direction.N) > 0) {
                        MapGrid[(mapGridY - 1), mapGridX] = Block.Empty;
                        emptySpaces.Add(new MapVector(mapGridX, mapGridY - 1));
                    }
                    if ((_directionGrid[y, x] & Direction.E) > 0)
                    {
                        MapGrid[mapGridY, (mapGridX + 1)] = Block.Empty;
                        emptySpaces.Add(new MapVector(mapGridX + 1, mapGridY));
                    }
                    if ((_directionGrid[y, x] & Direction.S) > 0)
                    {
                        MapGrid[(mapGridY + 1), mapGridX] = Block.Empty;
                        emptySpaces.Add(new MapVector(mapGridX, mapGridY + 1));
                    }
                    if ((_directionGrid[y, x] & Direction.W) > 0)
                    {
                        MapGrid[mapGridY, (mapGridX - 1)] = Block.Empty;
                        emptySpaces.Add(new MapVector(mapGridX - 1, mapGridY));
                    }
                }
            }

            pickPlayerAndGoalPositions(emptySpaces, deadEnds);

        }

        //Fills MapGrid with solid blocks
        private void fillWithSolid() {
            for (int x = 0; x < MapGrid.GetLength(0); x++)
            {
                for (int y = 0; y < MapGrid.GetLength(1); y++)
                {
                    MapGrid[x, y] = Block.Solid;
                }
            }
        }

        //Assigns random start position for Player and a position for the Goal
        //Also gives player a starting direction to face based on position
        private void pickPlayerAndGoalPositions(List<MapVector> emptySpaces, List<MapVector> deadEnds) {
            bool validPositions = false;
            Random rng = new Random();
            MapVector startPosition;
            Direction startDirection;
            while (!validPositions)
            {
                startPosition = emptySpaces[rng.Next(0, emptySpaces.Count)];
                if (MapGrid[startPosition.Y, startPosition.X + 1] == Block.Empty)
                {
                    startDirection = Direction.E;
                }
                else if (MapGrid[startPosition.Y + 1, startPosition.X] == Block.Empty)
                {
                    startDirection = Direction.S;
                }
                else if (MapGrid[startPosition.Y - 1, startPosition.X + 1] == Block.Empty)
                {
                    startDirection = Direction.N;
                }
                else
                {
                    startDirection = Direction.W;
                }
                Player = new Player(startPosition.X, startPosition.Y, MapGrid, startDirection);
                foreach (MapVector goalPosition in deadEnds)
                {
                    if ((Player.Position - goalPosition).Magnitude() >= (new MapVector(Width - 2, Height - 2).Magnitude() * 0.60))
                    {
                        Goal = goalPosition;
                        validPositions = true;
                        break;
                    }
                }
            }
        }

        public void CreateMap(int width, int height)
        {
            _directionGrid = _provider.CreateMap(width, height);
            CreateMap();
        }

        public void SaveDirectionMap(string path)
        {
            throw new NotImplementedException();
        }
    }
}
