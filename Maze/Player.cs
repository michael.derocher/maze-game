﻿namespace Maze
{
    public class Player : IPlayer
    {
        public Direction Facing { get; private set; }

        public MapVector Position { get; private set; }

        public int StartX { get; private set; }

        public int StartY { get; private set; }

        private Block[,] _mapGrid;

        public Player(int startX, int startY, Block[,] mapGrid, Direction facing) {
            if ((new MapVector(startX, startY).InsideBoundary(mapGrid.GetLength(1), mapGrid.GetLength(0))))
            {
                StartX = startX;
                StartY = startY;
                Position = new MapVector(startX, startY);
                _mapGrid = mapGrid;
                Facing = facing;
            }
            else {
                throw new ArgumentException("Starting position must be inside the map grid boundaries.");
            }
        }

        //Calculates the rotation in radians of the Player based on direction they're facing
        public float GetRotation()
        {
            switch (Facing)
            {
                case Direction.E:
                    return (float)Math.PI * (float)(1.0 / 2);
                case Direction.S:
                    return (float)Math.PI;
                case Direction.W:
                    return (float)Math.PI * (float)(3.0 /2);
                default:
                    return 0;
            }
        }

        //Moves the player forward in the direction they're facing if possible
        public void MoveBackward()
        {
            MapVector newPosition = Position - Facing;
            if (_mapGrid[newPosition.Y, newPosition.X] == Block.Empty) {
                Position = newPosition;
            }
        }

        //Moves the player backwards from the direction they're facing if possible
        public void MoveForward()
        {
            MapVector newPosition = Position + Facing;
            if (_mapGrid[newPosition.Y, newPosition.X] == Block.Empty)
            {
                Position = newPosition;
            }
        }

        //Turns the player to the right of direction they're facing
        public void TurnRight()
        {
            switch (Facing) {
                case Direction.N:
                    Facing = Direction.E;
                    break;
                case Direction.E:
                    Facing = Direction.S;
                    break;
                case Direction.S:
                    Facing = Direction.W;
                    break;
                case Direction.W:
                    Facing = Direction.N;
                    break;
            }
        }

        //Turns the player to the left of direction they're facing
        public void TurnLeft()
        {
            switch (Facing)
            {
                case Direction.N:
                    Facing = Direction.W;
                    break;
                case Direction.E:
                    Facing = Direction.N;
                    break;
                case Direction.S:
                    Facing = Direction.E;
                    break;
                case Direction.W:
                    Facing = Direction.S;
                    break;
            }
        }
    }
}
