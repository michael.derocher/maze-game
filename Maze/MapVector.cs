﻿namespace Maze
{
    public class MapVector : IMapVector
    {
        public bool IsValid { get; private set; }

        public int X { get; private set; }

        public int Y { get; private set; }

        public MapVector(int x, int y) {
            X = x;
            Y = y;
        }
        
        //Checks that the vector's position is within the provided width and height boundaries
        //Also updates IsValid property accordingly
        public bool InsideBoundary(int width, int height)
        {
            IsValid = ((X >= 0 && X < width) && (Y >= 0 && Y < height));
            return IsValid;
        }

        //Calculates the magnitude of the vector
        public double Magnitude()
        {
            return Math.Sqrt((X * X) + (Y * Y));
        }

        //Operator for addition of two vectors
        public static MapVector operator + (MapVector v1, MapVector v2) {
            return new MapVector(v1.X + v2.X, v1.Y + v2.Y);
        }

        //Operator for substraction of two vectors
        public static MapVector operator - (MapVector v1, MapVector v2) {
            return new MapVector(v1.X - v2.X, v1.Y - v2.Y);
        }

        //Scalar multiplication of a vector
        public static MapVector operator * (MapVector vector, int scalar) {
            return new MapVector(vector.X * scalar, vector.Y * scalar);
        }

        //Implicit conversion of Direction to a corresponding unit vector
        public static implicit operator MapVector(Direction direction) {
            switch (direction) {
                case Direction.N:
                    return new MapVector(0, -1);
                case Direction.E:
                    return new MapVector(1, 0);
                case Direction.S:
                    return new MapVector(0, 1);
                case Direction.W:
                    return new MapVector(-1, 0);
                default:
                    return new MapVector(0, 0);
            }
        }

        public override bool Equals(object? obj)
        {
            return obj is MapVector vector &&
                   X == vector.X &&
                   Y == vector.Y;
        }

    }
}
