﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Maze;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Maze.Tests
{
    [TestClass()]
    public class RecursiveMazeGeneratorTests
    {
        private Direction[,] _expected;

        [TestInitialize]
        public void TestSetup()
        {
            _expected = new Direction[,]{
                { Direction.S, Direction.E, Direction.E | Direction.W, Direction.S | Direction.W, Direction.E, Direction.E | Direction.W, Direction.E | Direction.W, Direction.S | Direction.W },
                { Direction.N | Direction.S, Direction.E | Direction.S, Direction.E | Direction.W, Direction.N | Direction.W, Direction.E | Direction.S, Direction.E | Direction.W, Direction.S | Direction.W, Direction.N | Direction.S },
                { Direction.N | Direction.S, Direction.N | Direction.E, Direction.E | Direction.W, Direction.S | Direction.W, Direction.N | Direction.E, Direction.S | Direction.W, Direction.N | Direction.S, Direction.N | Direction.S},
                { Direction.N | Direction.E | Direction.S, Direction.E | Direction.W, Direction.S | Direction.W, Direction.N | Direction.S, Direction.E | Direction.S, Direction.N | Direction.W, Direction.N | Direction.S, Direction.N | Direction.S },
                { Direction.N | Direction.S, Direction.S, Direction.N | Direction.E, Direction.N | Direction.W, Direction.N | Direction.E, Direction.S | Direction.W, Direction.N | Direction.E, Direction.N | Direction.S | Direction.W },
                { Direction.N | Direction.E, Direction.N | Direction.E | Direction.W, Direction.E | Direction.W, Direction.E | Direction.W, Direction.E | Direction.W, Direction.N | Direction.W, Direction.E, Direction.N | Direction.W }
            };
        }

        [TestMethod()]
        public void CreateMapTest_Default()
        {
            RecursiveMazeGenerator generator = new RecursiveMazeGenerator(1);
            Direction[,] result = generator.CreateMap();

            Assert.IsNotNull(result);
            Assert.AreEqual(6, result.GetLength(0));
            Assert.AreEqual(8, result.GetLength(1));
            AssertAllPositionsNotNone(result);
            AssertBordersDontPointOutwards(result);
            AssertGridsAreEqual(_expected, result);
        }

        [DataTestMethod()]
        [DataRow(-7, 6)]
        [DataRow(3, -9)]
        [DataRow(-8, -1)]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateMapTest_CustomParameters_InvalidInputs(int width, int height)
        {
            RecursiveMazeGenerator generator = new RecursiveMazeGenerator(1);

            generator.CreateMap(width, height); //Should throw exception

            Assert.Fail();
        }

        [TestMethod()]
        public void CreateMapTest_CustomParameters()
        {
            RecursiveMazeGenerator generator = new RecursiveMazeGenerator(1);
            Direction[,] result = generator.CreateMap(8,6);

            Assert.IsNotNull(result);
            Assert.AreEqual(6, result.GetLength(0));
            Assert.AreEqual(8, result.GetLength(1));
            AssertAllPositionsNotNone(result);
            AssertBordersDontPointOutwards(result);
            AssertGridsAreEqual(_expected, result);
        }

        private void AssertAllPositionsNotNone(Direction[,] grid)
        {
            for (int y = 0; y < grid.GetLength(0); y++)
            {
                for (int x = 0; x < grid.GetLength(1); x++)
                {
                    Assert.AreNotEqual(Direction.None, grid[y, x]);
                }
            }
        }

        private void AssertBordersDontPointOutwards(Direction[,] grid)
        {
            int gridWidth = grid.GetLength(1);
            int gridHeight = grid.GetLength(0);
            for (int y = 0; y < gridHeight; y++)
            {
                Assert.IsFalse((grid[y, 0] & Direction.W) > 0);
                Assert.IsFalse((grid[y, gridWidth - 1] & Direction.E) > 0);
            }

            for (int x = 0; x < gridWidth; x++)
            {
                Assert.IsFalse((grid[0, x] & Direction.N) > 0);
                Assert.IsFalse((grid[gridHeight - 1, x] & Direction.S) > 0);
            }
        }

        private void AssertGridsAreEqual(Direction[,] expected, Direction[,] actual)
        {
            Assert.AreEqual(expected.GetLength(0), actual.GetLength(0));
            Assert.AreEqual(expected.GetLength(1), actual.GetLength(1));
            for (int y = 0; y < actual.GetLength(0); y++)
            {
                for (int x = 0; x < actual.GetLength(1); x++)
                {
                    Assert.AreEqual(expected[y, x], actual[y, x]);
                }
            }
        }
    }
}