﻿using Moq;

namespace Maze.Tests
{
    [TestClass()]
    public class MapTests
    {
        [TestMethod()]
        public void MapTest()
        {
            var provider = new Mock<IMapProvider>();
            Map map = new Map(provider.Object);

            Assert.IsNotNull(map);
        }

        [TestMethod()]
        public void CreateMapTest()
        {
            Direction[,] directionGrid = new Direction[,]{
                { Direction.E | Direction.S, Direction.W | Direction.S },
                { Direction.N, Direction.N }
            };
            var provider = new Mock<IMapProvider>();
            provider.Setup(p => p.CreateMap()).Returns(directionGrid);
            Map map = new Map(provider.Object);
            Block[,] expected = new Block[,]
            {
                { Block.Solid, Block.Solid, Block.Solid, Block.Solid, Block.Solid },
                { Block.Solid, Block.Empty, Block.Empty, Block.Empty, Block.Solid },
                { Block.Solid, Block.Empty, Block.Solid, Block.Empty, Block.Solid },
                { Block.Solid, Block.Empty, Block.Solid, Block.Empty, Block.Solid },
                { Block.Solid, Block.Solid, Block.Solid, Block.Solid, Block.Solid }
            };

            map.CreateMap();

            Assert.IsNotNull(map);
            for (int y = 0; y < expected.GetLength(0); y++) { 
                for (int x = 0; x < expected.GetLength(1); x++) {
                    Assert.AreEqual(expected[y,x], map.MapGrid[y,x]);
                }
            }

            Assert.IsNotNull(map.Player);
            Assert.IsNotNull(map.Goal);
            Assert.IsFalse(map.IsGameFinished);
            Assert.AreEqual(expected.GetLength(1), map.Width);
            Assert.AreEqual(expected.GetLength(0), map.Height);
        }

    }
}