﻿namespace Maze.Tests
{
    [TestClass()]
    public class PlayerTests
    {
        private Block[,] _mapGrid;

        [TestInitialize] public void TestSetup()
        {
            _mapGrid = new Block[,]
            {
                { Block.Solid, Block.Solid, Block.Solid, Block.Solid, Block.Solid },
                { Block.Solid, Block.Empty, Block.Empty, Block.Empty, Block.Solid },
                { Block.Solid, Block.Empty, Block.Solid, Block.Empty, Block.Solid },
                { Block.Solid, Block.Empty, Block.Solid, Block.Empty, Block.Solid },
                { Block.Solid, Block.Solid, Block.Solid, Block.Solid, Block.Solid }
            };
        }


        [TestMethod()]
        public void PlayerTest()
        {
            Player p = new Player(1,2,_mapGrid, Direction.S);

            Assert.IsNotNull(p);
            Assert.AreEqual(1, p.StartX);
            Assert.AreEqual(2, p.StartY);
            Assert.AreEqual(Direction.S, p.Facing);
        }

        [TestMethod()]
        public void PlayerTest_Fails() 
        {
            Player p;

            try {
                p = new Player(8, 9, _mapGrid, Direction.S);
                Assert.Fail();
            } catch (ArgumentException e)
            {
                Assert.AreEqual("Starting position must be inside the map grid boundaries.", e.Message);
            }
        }

        [DataTestMethod()]
        [DataRow(Direction.N, 0)]
        [DataRow(Direction.E, (float)Math.PI * (float)(1.0 / 2))]
        [DataRow(Direction.S, (float)Math.PI)]
        [DataRow(Direction.W, (float)Math.PI * (float)(3.0 / 2))]
        public void GetRotationTest(Direction facing, float expectedRadians)
        {
            Player p = new Player(1, 1, _mapGrid, facing);

            float result = p.GetRotation();

            Assert.AreEqual(expectedRadians, result);
        }

        [TestMethod()]
        public void MoveBackwardTest_Valid()
        {
            Player p1 = new Player(2, 1, _mapGrid, Direction.E);

            p1.MoveBackward();

            Assert.AreEqual(1, p1.Position.X);
            Assert.AreEqual(1, p1.Position.Y);
        }

        [TestMethod()]
        public void MoveBackwardTest_Invalid()
        {
            Player p1 = new Player(1, 1, _mapGrid, Direction.E);

            p1.MoveBackward();

            Assert.AreEqual(1, p1.Position.X);
            Assert.AreEqual(1, p1.Position.Y);
        }

        [TestMethod()]
        public void MoveForwardTest_Valid()
        {
            Player p1 = new Player(1, 1, _mapGrid, Direction.E);

            p1.MoveForward();

            Assert.AreEqual(2, p1.Position.X);
            Assert.AreEqual(1, p1.Position.Y);
        }

        [TestMethod()]
        public void MoveForwardTest_Invalid()
        {
            Player p1 = new Player(1, 1, _mapGrid, Direction.W);

            p1.MoveForward();

            Assert.AreEqual(1, p1.Position.X);
            Assert.AreEqual(1, p1.Position.Y);
        }

        [DataTestMethod()]
        [DataRow(Direction.N, Direction.W)]
        [DataRow(Direction.E, Direction.N)]
        [DataRow(Direction.S, Direction.E)]
        [DataRow(Direction.W, Direction.S)]
        public void TurnLeftTest(Direction facing, Direction left)
        {
            Player p = new Player(1, 1, _mapGrid, facing);

            p.TurnLeft();

            Assert.AreEqual(left, p.Facing);
        }

        [DataTestMethod()]
        [DataRow(Direction.N, Direction.E)]
        [DataRow(Direction.E, Direction.S)]
        [DataRow (Direction.S, Direction.W)]
        [DataRow (Direction.W, Direction.N)]
        public void TurnRightTest(Direction facing, Direction right)
        {
            Player p = new Player(1, 1, _mapGrid, facing);

            p.TurnRight();

            Assert.AreEqual(right, p.Facing);
        }
    }
}