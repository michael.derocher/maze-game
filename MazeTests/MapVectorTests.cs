﻿namespace Maze.Tests
{
    [TestClass()]
    public class MapVectorTests
    {
        [TestMethod()]
        public void MapVectorTest()
        {
            MapVector vector = new MapVector(1,2);

            Assert.IsNotNull(vector);
            Assert.AreEqual(1, vector.X);
            Assert.AreEqual(2, vector.Y);
        }

        [TestMethod()]
        public void InsideBoundaryTest_Valid()
        {
            MapVector vector = new MapVector(1, 2);
            bool valid;

            valid = vector.InsideBoundary(5,5);

            Assert.IsNotNull(valid);
            Assert.IsNotNull(vector.IsValid);
            Assert.IsTrue(valid);
            Assert.IsTrue(vector.IsValid);
        }

        [DataTestMethod()]
        [DataRow(7,6)]
        [DataRow(3,9)]
        [DataRow(8,1)]
        [DataRow(-1,2)]
        [DataRow(1,-3)]
        public void InsideBoundaryTest_Invalid(int X, int Y)
        {
            MapVector vector = new MapVector(X, Y);
            bool valid;

            valid = vector.InsideBoundary(5, 5);

            Assert.IsNotNull(valid);
            Assert.IsNotNull(vector.IsValid);
            Assert.IsFalse(valid);
            Assert.IsFalse(vector.IsValid);
        }

        [TestMethod()]
        public void MagnitudeTest()
        {
            MapVector vector = new MapVector(3, 4);
            double magnitude;

            magnitude = vector.Magnitude();

            Assert.IsNotNull(magnitude);
            Assert.AreEqual(5, magnitude);
        }

        [TestMethod()]
        public void EqualsTest_True()
        {
            MapVector v1 = new MapVector(1, 2);
            MapVector v2 = new MapVector(1, 2);

            Assert.IsTrue(v1.Equals(v2));
        }

        [DataTestMethod()]
        [DataRow(5,2)]
        [DataRow(1,5)]
        public void EqualsTest_False(int X, int Y)
        {
            MapVector v1 = new MapVector(X, Y);
            MapVector v2 = new MapVector(1, 2);

            Assert.IsFalse(v1.Equals(v2));
        }

        [TestMethod()]
        public void EqualsTest_False_DifferentObject() {
            MapVector v = new MapVector(1, 2);
            String str = "Hello";

            Assert.IsFalse(v.Equals(str));
        }

        [TestMethod()]
        public void PlusOperatorTest()
        {
            MapVector v1 = new MapVector(3, 3);
            MapVector v2 = new MapVector(1, 2);
            MapVector result;

            result = v1 + v2;

            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.X);
            Assert.AreEqual(5, result.Y);
        }

        [TestMethod()]
        public void MinusOperatorTest()
        {
            MapVector v1 = new MapVector(3, 3);
            MapVector v2 = new MapVector(1, 2);
            MapVector result;

            result = v1 - v2;

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.X);
            Assert.AreEqual(1, result.Y);
        }

        [TestMethod()]
        public void MultiplicationOperatorTest()
        {
            MapVector v1 = new MapVector(1, 2);
            MapVector result;

            result = v1 * 5;

            Assert.IsNotNull(result);
            Assert.AreEqual(5, result.X);
            Assert.AreEqual(10, result.Y);
        }

        [DataTestMethod()]
        [DataRow(Direction.N, 0, -1)]
        [DataRow(Direction.S, 0, 1)]
        [DataRow(Direction.E, 1, 0)]
        [DataRow(Direction.W, -1, 0)]
        [DataRow(Direction.None, 0, 0)]
        public void ImplicitDirectionConversionTest(Direction direction, int expectedX, int expectedY)
        {
            MapVector v = direction;
            MapVector expected = new MapVector(expectedX, expectedY);

            Assert.AreEqual(expected, v);
        }
    }
}